## Импортировать настройки в *phpstorm* ##
* Файл settings_05.07.15.jar
* Как настроить phpstorm - на сайте *http://www.sitepoint.com/install-xdebug-phpstorm-vagrant/*
* Статья в Evernote - [Xdebug в phpstorm на ubuntu](https://www.evernote.com/shard/s200/sh/8dbb46d4-7a85-4562-98a0-7ee20b2bfb2d/0a08a35dafa5bf9ce4a0fb492b4936ee)

## Настройка на виртуальном сервере ##
- cd ~ перейти в корневую папку
- cd followfoxy/vm/up-project/ для общения с vagrant
- vagrant up запустить виртуальный сервер
- vagrant ssh перейти в этот сервер
- sudo apt-get install php-xdebug
- добавить данные в файл - 20-xdebug.ini
*sudo nano /etc/php5/fpm/conf.d/20-xdebug.ini* следующие строки:
```
#!html
zend_extension=/usr/lib/php5/20100525+lfs/xdebug.so
xdebug.remote_enable = on
xdebug.remote_connect_back = on
xdebug.idekey = vagrant
xdebug.remote_port=9000
```
* перезагрузить php - *sudo service php5-fpm restart*

## Настройка IDE ##
![xdebug_phpstorm.png](https://bitbucket.org/repo/MLzpAL/images/3939049441-xdebug_phpstorm.png)

## Настройка в браузере ##
* Установить в Chrome *xdebug helper*
* Запустить